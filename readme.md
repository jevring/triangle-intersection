# Triangle intersection
This project was created to find the intersection of two triangles. The underlying idea was that, if I could turn two 
triangles into multiple ones, I could avoid rendering the overlap in another 3d engine project I was working on.
I'd say it works in something like 90% of the cases I tried, but I couldn't quite get it to work in 100% of the cases, 
so progress stagnated.

If successful, the hope would be that I could have integrated it into my cpu 3d renderer [here](https://bitbucket.org/jevring/threedee)