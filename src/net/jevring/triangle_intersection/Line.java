/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Line {
	public final Point p1;
	public final Point p2;

	public Line(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}

	public Intersection intersection(Line line) {
		// thanks to http://hg.postspectacular.com/toxiclibs/src/44d9932dbc9f9c69a170643e2d459f449562b750/src.core/toxi/geom/Line2D.java?at=default
		// http://geomalgorithms.com/a05-_intersect-1.html
		// http://paulbourke.net/geometry/pointlineplane/
		// calculate the lowest common denominator, which is effectively the parts times each other.
		// it's not the lowest, perhaps, but it's guaranteed to be correct
		double denominator = (line.p2.y - line.p1.y) * (p2.x - p1.x) - (line.p2.x - line.p1.x) * (p2.y - p1.y);
		// todo: what is the semantic of these?
		double numeratorA = (line.p2.x - line.p1.x) * (p1.y - line.p1.y) - (line.p2.y - line.p1.y) * (p1.x - line.p1.x);
		double numeratorB = (p2.x - p1.x) * (p1.y - line.p1.y) - (p2.y - p1.y) * (p1.x - line.p1.x);


		if (denominator != 0) {
			// todo: what's the semantic of these?
			double ua = numeratorA / denominator;
			double ub = numeratorB / denominator;
			Point i = p1.multiply(p2, ua);
			if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1) {
				// intersection of the SEGMENT
				return new Intersection(i.x, i.y, this, line, ua, ub);
			} else {
				// intersection on the infinite lines
				return null;
			}
		} else {
			// todo: perhaps more information here later
			return null;
		}
	}

	@Override
	public String toString() {
		return "Line[" + System.identityHashCode(this) + "]{" +
				"p1=" + p1 +
				", p2=" + p2 +
				'}';
	}
}
