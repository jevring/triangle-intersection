/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class BackLeadSubtractor implements TriangleSubtractor {
	@Override
	public TriangleSubtraction subtract(Triangle back, Triangle front) {
		/*
		From the point of view of a 'back' corner, we can help form 1, 2 or 3 triangles:
		1: both near legs have intersections with the SAME base line
		2: 1 intersection, or both near legs have intersections with DIFFERENT base lines
		3: no intersections at all
		
		Always go for the nearest intersection to form a triangle.
		
		But how do we deal with the fact that ALL corners will want to form triangles. That makes up to 9, which is 3 too many.
		We could say that each corner that can form a certain TYPE of triangle should, unless it's already been formed by another corner.
		For instance, these types could be front-corner-back-corner, intersection-intersection, intersection-front-corner, etc
		
		Always use the nearest intersection on the near legs (simple selection, but we still need the distance)
		Always use the 1 (or 2, depending on the triangle type) closest front corners		
		 */

		return null;
	}
}
