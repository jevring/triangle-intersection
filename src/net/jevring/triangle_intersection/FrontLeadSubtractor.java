/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class FrontLeadSubtractor implements TriangleSubtractor {
	@Override
	public TriangleSubtraction subtract(Triangle back, Triangle front) {
		// todo: when a 'back' triangle has been processed, we should still use the WHOLE triangle as an occluder in the next step.
		// this is to avoid having more and more triangles for intersections.

		List<Triangle> triangles = new ArrayList<>();
		List<Intersection> intersections = new ArrayList<>();

		List<Point> frontPoints = new ArrayList<>();
		List<Point> backPoints = new ArrayList<>();

		// todo: in the real world, only do this if the bounding box is sufficiently large.
		// otherwise the cost of performing these calculations will be higher than simply redrawing things

		Point op1 = front.getP1();
		Point op2 = front.getP2();
		Point op3 = front.getP3();

		Point p1 = back.getP1();
		Point p2 = back.getP2();
		Point p3 = back.getP3();

		Line ol1 = new Line(op1, op2);
		Line ol2 = new Line(op2, op3);
		Line ol3 = new Line(op3, op1);

		List<Line> lines = Arrays.asList(ol1, ol2, ol3);

		boolean op1Inside = Triangle.inside(op1, p1, p2, p3);

		boolean op2Inside = Triangle.inside(op2, p1, p2, p3);

		boolean op3Inside = Triangle.inside(op3, p1, p2, p3);

		boolean p1Inside = Triangle.inside(p1, op1, op2, op3);

		boolean p2Inside = Triangle.inside(p2, op1, op2, op3);

		boolean p3Inside = Triangle.inside(p3, op1, op2, op3);


		if (!p1Inside) {
			backPoints.add(p1);
		}
		if (!p2Inside) {
			backPoints.add(p2);
		}
		if (!p3Inside) {
			backPoints.add(p3);
		}

		if (backPoints.isEmpty()) {
			// all the back points are hidden behind the front triangle.
			// this means that the back triangle shouldn't be rendered at all.
			return new TriangleSubtraction(triangles, intersections);
		}

		Line l1 = new Line(p1, p2);
		Line l2 = new Line(p2, p3);
		Line l3 = new Line(p3, p1);

		Intersection i11 = ol1.intersection(l1);
		if (i11 != null) {
			frontPoints.add(i11);
			intersections.add(i11);
		}
		Intersection i21 = ol1.intersection(l2);
		if (i21 != null) {
			frontPoints.add(i21);
			intersections.add(i21);
		}
		Intersection i31 = ol1.intersection(l3);
		if (i31 != null) {
			frontPoints.add(i31);
			intersections.add(i31);
		}

		Intersection i12 = ol2.intersection(l1);
		if (i12 != null) {
			frontPoints.add(i12);
			intersections.add(i12);
		}
		Intersection i22 = ol2.intersection(l2);
		if (i22 != null) {
			frontPoints.add(i22);
			intersections.add(i22);
		}
		Intersection i32 = ol2.intersection(l3);
		if (i32 != null) {
			frontPoints.add(i32);
			intersections.add(i32);
		}
		Intersection i13 = ol3.intersection(l1);
		if (i13 != null) {
			frontPoints.add(i13);
			intersections.add(i13);
		}
		Intersection i23 = ol3.intersection(l2);
		if (i23 != null) {
			frontPoints.add(i23);
			intersections.add(i23);
		}
		Intersection i33 = ol3.intersection(l3);
		if (i33 != null) {
			frontPoints.add(i33);
			intersections.add(i33);
		}

		// todo: check what happens then the front is larger than the back, and can disable 2 back points

		// todo: don't use back points that are not visible

		int occludedPointsInside = 0;

		// occluder starting points
		if (op1Inside) {
			occludedPointsInside++;
			createTriangle(triangles, intersections, frontPoints, backPoints, op1, Color.RED, ol2, ol3, ol1);
		}

		if (op2Inside) {
			occludedPointsInside++;
			createTriangle(triangles, intersections, frontPoints, backPoints, op2, Color.GREEN, ol3, ol1, ol2);
		}

		if (op3Inside) {
			occludedPointsInside++;
			createTriangle(triangles, intersections, frontPoints, backPoints, op3, Color.BLUE, ol1, ol2, ol3);
		}

		// back starting points
		if (!p1Inside && frontPoints.size() > 1) {
			createTriangle(triangles, frontPoints, p1, lines, Color.BROWN, occludedPointsInside);
		}
		if (!p2Inside && frontPoints.size() > 1) {
			createTriangle(triangles, frontPoints, p2, lines, Color.AZURE, occludedPointsInside);
		}
		if (!p3Inside && frontPoints.size() > 1) {
			createTriangle(triangles, frontPoints, p3, lines, Color.BLUEVIOLET, occludedPointsInside);
		}
		return new TriangleSubtraction(triangles, intersections);
	}

	private void createTriangle(List<Triangle> triangles,
	                            List<Point> frontPoints,
	                            Point backPoint,
	                            List<Line> lines,
	                            Color fill,
	                            int occludedPointsInside) {
		Collections.sort(frontPoints, (o1, o2) -> Double.compare(backPoint.distance(o1), backPoint.distance(o2)));
		// only do this if this Intersection isn't hidden by the front triangle
		Iterator<Point> fpi = frontPoints.iterator();
		Point op0 = fpi.next();
		Point op1 = fpi.next();

		if (fpi.hasNext() && sameIntersectingLine(op0, op1)) {
			// as there can only be 2 intersections on the same intersecting line, we only need to check this once
			op1 = fpi.next();
		}

		Line longestLine = new Line(backPoint, op1);
		if (fpi.hasNext() && op1 instanceof Intersection) {
			Intersection i = (Intersection) op1;
			for (Line line : lines) {
				if (line != i.getBaseLine()) {
					if (longestLine.intersection(line) != null) {
						op1 = fpi.next();
						break;
					}
				}
			}
		}
		if (false && fpi.hasNext() && occludedPointsInside == 1) {
			// todo: this is ugly, but it might save us
			for (Triangle triangle : triangles) {
				for (Line line : triangle.getLines()) {
					Intersection i = longestLine.intersection(line);
					if (i != null && !i.isAtEnd()) {
						// todo: this happens for all the things, and all hell breaks loose.
						// but of course it does, the intersection is at the point!
						// we need to check if there was an intersection that is NOT on the edge. i.e 'ua' and 'ub' need to be strictly between 0 and 1, not inclusive
						System.out.println("tttt: " + i.isAtEnd());
						op1 = fpi.next();
						break;
					}
				}
			}
		}

		triangles.add(new Triangle(backPoint, op0, op1, fill));
	}

	private boolean sameIntersectingLine(Point op0, Point op1) {
		return op0 instanceof Intersection && op1 instanceof Intersection && ((Intersection) op0).getIntersectingLine() == ((Intersection) op1).getIntersectingLine();
	}

	private void createTriangle(List<Triangle> triangles,
	                            List<Intersection> intersections,
	                            List<Point> frontPoints,
	                            List<Point> backPoints,
	                            Point op,
	                            Paint fill,
	                            Line oppositeLine,
	                            Line left,
	                            Line right) {
		// todo: maybe this shouldn't handle intersections? maybe that will be handled by the other triangles anyway?
		frontPoints.add(op);

		Collections.sort(backPoints, (o1, o2) -> Double.compare(op.distance(o1), op.distance(o2)));
		Point c1 = backPoints.get(0);
		Point c2;
		if (backPoints.size() == 1) {
			Collections.sort(intersections, (o1, o2) -> Double.compare(op.distance(o1), op.distance(o2)));
			System.out.println("Choosing from intersections"); // todo: make sure we test this
			c2 = intersections.get(0);
		} else {
			c2 = backPoints.get(1);
		}
		// if there's an intersection with the line opposite of the Intersection in the triangle, find a better candidate

		Line line1 = new Line(op, c1);
		Line line2 = new Line(op, c2);
		Line base = new Line(c1, c2);
		if (oppositeLine.intersection(line1) != null) {
			System.out.println("Intersection with opposite for l1");
			if (backPoints.size() == 1) {
				c1 = intersections.get(1);
			} else {
				c1 = backPoints.get(2);
			}
		} else if (oppositeLine.intersection(line2) != null) {
			System.out.println("Intersection with opposite for l2");
			if (backPoints.size() == 1) {
				c2 = intersections.get(1);
			} else {
				c2 = backPoints.get(2);
			}
		} else {
			System.out.println("No intersection with opposite");

			if (right.intersection(base) != null || left.intersection(base) != null) {
				// todo: we can simply abort when we encounter this scenario and render the entire triangle.
				// it should be rare enough that it doesn't become an issue
				System.out.println("Intersection with near line");
				// todo: it doesn't seem to matter what I do here, or what I assign to what
				// no matter what I assign to c2, it ends up being the bottom for the blue
				// no matter what I assign to c1, it ends up being the left for the blue.
				// WTF?!
				// the problem seems to be which back Intersection is closest.
				// whenever 50,300 is closest, everything breaks down.
				// under what conditions does this happen? How can we check it here?
				// if we switch to 1, 2, then it'll just be broken in the inverse conditions
				// I think the fact that we're using "closest" in place of "best suitable" is the issue.
				// of course, I'm not sure what other indicator we could use for this
				System.out.println("backPoints.get(0) = " + backPoints.get(0));
				System.out.println("backPoints.get(1) = " + backPoints.get(1));
				System.out.println("backPoints.get(2) = " + backPoints.get(2));
				c2 = backPoints.size() == 1 ? intersections.get(1) : backPoints.get(2);
			} else {
				System.out.println("No intersection with near");
			}

		}

		triangles.add(new Triangle(op, c1, c2, fill));
	}
}
