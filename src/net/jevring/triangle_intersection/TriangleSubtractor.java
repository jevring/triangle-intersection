/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public interface TriangleSubtractor {
	
	
	// todo: embody the different implementations, as well as running information, in these.
	// alternatively embody the running information in some other object, so that things can be executed in parallel
	
	/**
	 * Determines which triangles should be visible after the {@code front} occludes this triangle.
	 * If the front does not occlude this triangle, we return a list with this triangle.
	 * If the front completely occludes this triangle, we return an empty list.
	 * If there is partial occlusion, we return a list of triangles to <b>replace</b> this triangle.
	 * <p>
	 * The front is always untouched, and never included in the output.
	 * <p>
	 * We should never render this triangle itself. We should only render the triangles in the list returned
	 * from here. The only exception is when it is the only value in the list, as above.
	 *
	 * @param front the occluding triangle, i.e. the one "in front" of this triangle
	 * @return a list of triangles to be rendered.
	 */
	public TriangleSubtraction subtract(Triangle back, Triangle front);
}
