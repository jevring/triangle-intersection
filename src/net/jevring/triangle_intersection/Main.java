/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

import com.sun.org.apache.xpath.internal.SourceTree;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Main extends Application {
	private final LongAdder events = new LongAdder();
	private final List<Circle> circles = new ArrayList<>();
	private final List<Triangle> replacementTriangles = new ArrayList<>();
	private final StackPane root = new StackPane();
	private final TriangleSubtractor subtractor = new FrontLeadSubtractor();

	@Override
	public void start(Stage stage) throws Exception {

		Triangle front = new Triangle(0, 10, 10, 200, 150, 150);
		front.setManaged(false);
		front.setFill(Color.GRAY);

		Triangle back = new Triangle(400, 600, 400, 100, 50, 300);
		back.setManaged(false);

		
		BorderPane bp = new BorderPane();
		Label coordinates = new Label();
		bp.setBottom(coordinates);
		bp.setCenter(root);
		
		
		
		root.getChildren().addAll(back, front);
		front.setOnDragDetected(event -> {
			// start the drag'n'drop sequence
			Dragboard dragboard = root.startDragAndDrop(TransferMode.MOVE);

			// this is just a glorified map
			ClipboardContent clipboardContent = new ClipboardContent();
			// without content, the drag sequence never happens. We never get the OnDragOver event
			clipboardContent.putString("front");

			dragboard.setContent(clipboardContent);
			event.consume();
		});
		root.setOnMouseMoved(event -> coordinates.setText(event.getX() + ", " + event.getY()));
		root.setOnDragOver(event -> {
			// if we don't get this transform here, we can hover over the board but not over other pieces
			double xTransform = root.getLocalToParentTransform().getTx();
			double yTransform = root.getLocalToParentTransform().getTy();
			double xTarget = event.getX() + xTransform;
			double yTarget = event.getY() + yTransform;

			double xPieceOffset = (front.getLayoutBounds().getWidth() / 2d);
			double yPieceOffset = (front.getLayoutBounds().getHeight() / 2d);

			double x = xTarget - xPieceOffset;
			double y = yTarget - yPieceOffset;
			front.relocate(x, y);
			// todo: have a flag to simply switch the occluder
			checkInside(back, front);
			event.consume();
		});
		Scene scene = new Scene(bp, 500, 768);

		stage.setTitle("Triangle intersection");
		stage.setScene(scene);
		stage.show();
	}

	private void checkInside(Triangle back, Triangle front) {
		events.increment();
		boolean p1Inside = back.contains(front.getP1());
		boolean p2Inside = back.contains(front.getP2());
		boolean p3Inside = back.contains(front.getP3());

		boolean p1Hidden = front.contains(back.getP1());
		boolean p2Hidden = front.contains(back.getP2());
		boolean p3Hidden = front.contains(back.getP3());
		if (p1Inside && p2Inside && p3Inside) {
			// todo: cut new triangles
		} else if (p1Hidden && p2Hidden && p3Hidden) {
			// todo: the rear triangle is completely hidden. Don't draw it
		}
		System.out.printf("%d: p1 = %b, p2 = %b, p3 = %b, p1.x = %4.0f, p1.y = %4.0f, inside = %b\n",
		                  events.intValue(), p1Inside, p2Inside, p3Inside,
		                  front.getP1().x,
		                  front.getP1().y,
		                  p1Inside && p2Inside && p3Inside);
		
		root.getChildren().removeAll(replacementTriangles);
		replacementTriangles.clear();
		long start = System.nanoTime();
		TriangleSubtraction results = subtractor.subtract(back, front);
		long end = System.nanoTime();
		
		System.out.println("Triangle intersection in " + (end - start) + " nanoseconds");
		List<Triangle> triangles = results.getTriangles();
		double i = 30;
		for (Triangle triangle : triangles) {
			triangle.setManaged(false);
			//System.out.println("i = " + i);
			//triangle.setFill(Color.hsb(i, 1, 1));
			root.getChildren().add(triangle);
			replacementTriangles.add(triangle);
			i += 45;
		}
		
		root.getChildren().removeAll(circles);
		circles.clear();
		List<Intersection> intersections = results.getIntersections();
		for (Point intersection : intersections) {
			Circle c = new Circle(intersection.x, intersection.y, 5, Color.RED);
			c.setManaged(false);
			root.getChildren().add(c);
			circles.add(c);
		}
	}

}
