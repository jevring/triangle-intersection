/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.triangle_intersection;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.util.Pair;

import java.util.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Triangle extends Polygon {
	public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
		super.getPoints().addAll(x1, y1, x2, y2, x3, y3);
	}

	public Triangle(Point p1, Point p2, Point p3, Paint fill) {
		super.getPoints().addAll(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
		setFill(fill);
	}

	private Point newPoint(int x, int y) {
		Double cx = getPoints().get(x);
		Double cy = getPoints().get(y);
		return new Point(cx + getLayoutX(), cy + getLayoutY());
	}

	public Point getP1() {
		return newPoint(0, 1);
	}

	public Point getP2() {
		return newPoint(2, 3);
	}

	public Point getP3() {
		return newPoint(4, 5);
	}
	
	public List<Line> getLines() {
		Point p1 = getP1();
		Point p2 = getP2();
		Point p3 = getP3();
		
		return Arrays.asList(new Line(p1, p2), new Line(p2, p3), new Line(p3, p1));
		
	}

	public boolean contains(Point p) {
		// this is just the square in which it's contained. that's a good starting point
		//System.out.println("containsBounds(x, y) = " + containsBounds(x, y));
		// https://stackoverflow.com/questions/2049582/how-to-determine-a-point-in-a-triangle/2049593#2049593
		// todo: have a fast out here, to avoid doing more calculations than are necessary
		Point p1 = getP1();
		Point p2 = getP2();
		Point p3 = getP3();

		boolean b1 = sign(p, p1, p2) < 0d;
		boolean b2 = sign(p, p2, p3) < 0d;
		boolean b3 = sign(p, p3, p1) < 0d;

		return (b1 == b2) && (b2 == b3);
	}

	public static double sign(Point p, Point v1, Point v2) {
		return (p.x - v2.x) * (v1.y - v2.y) - (v1.x - v2.x) * (p.y - v2.y);
	}

	public static boolean inside(Point op1, Point p1, Point p2, Point p3) {
		// todo: can we use this data to say which side of the line the 'back' point is, and then use that?
		boolean op1p1p2 = sign(op1, p1, p2) < 0d;
		boolean op1p2p3 = sign(op1, p2, p3) < 0d;
		boolean op1p3p1 = sign(op1, p3, p1) < 0d;
		return (op1p1p2 == op1p2p3) && (op1p2p3 == op1p3p1);
	}
}

